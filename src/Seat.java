public class Seat {

  /*
  * S: Student
  * X: Sold
  * O: Available
  * */
  private char status;

  public Seat() {
    status = 'O';
  }

  public char getStatus() {
    return status;
  }

  public void setStatus(char status) {
    this.status = status;
  }

}