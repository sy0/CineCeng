public class Saloon {

  /*
  * Every saloon can have only a movie.
  * *** it doesn't clone the movie object,
  * it is only another pointer of same object. ***
  * */
  private Movie movie;

  /*
  * Each saloon has 3 showtimes which are morning, noon and evening.
  * */
  private Showtime morning = new Showtime();
  private Showtime noon = new Showtime();
  private Showtime evening = new Showtime();

  public Movie getMovie() {
    return movie;
  }

  public void setMovie(Movie movie) {
    this.movie = movie;
  }

  /*
  * Resetting the movie in EndTheDay operations
  * */
  public void resetMovie() {
    movie = null;
  }

  /*
  * Returns the showtime by the name
  * */
  public Showtime getShowtime(String showtime) {
    if (showtime.equalsIgnoreCase("morning")) {
      return morning;
    } else if (showtime.equalsIgnoreCase("noon")) {
      return noon;
    }
    return evening;
  }

}
