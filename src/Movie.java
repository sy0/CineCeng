public class Movie {

  private int movieID;
  private String title;
  private String genre;
  private int duration;
  private Director director;
  /*
  * Stars of the movie is not known
  * So it is decided in constructor by the incoming parameter
  * */
  private Star[] stars;
  /*
  * To calculate revenue for each movie
  * */
  private double revenue;

  public Movie(String title, String genre, int duration, Director director, Star[] stars) {
    this.title = title;
    this.genre = genre;
    this.duration = duration;
    this.director = director;
    this.stars = stars;
  }

  public String getTitle() {
    return title;
  }

  public String getGenre() {
    return genre;
  }

  public int getDuration() {
    return duration;
  }

  public int getMovieID() {
    return movieID;
  }

  public Director getDirector() {
    return director;
  }

  public Star[] getStars() {
    return stars;
  }

  public void setMovieID(int movieID) {
    this.movieID = movieID;
  }

  public double getRevenue() {
    return revenue;
  }

  /*
  * Reseting revenues from all movies in EndTheDay operations
  * */
  public void resetRevenue() {
    revenue = 0;
  }

  /*
  * When a ticket sold, revenue of that movie has to increase by this method
  * */
  public void increaseRevenue(double ticketPrice) {
    revenue += ticketPrice;
  }

  /*
   * When a ticket cancelled, revenue of that movie has to decrease by this method
   * */
  public void decreaseRevenue(double ticketPrice) {
    revenue -= ticketPrice;
  }

}
