import java.util.Random;

public class Showtime {

  /*
  * Each showtime has a 5x5 seats array.
  * */
  private Seat[][] seats = new Seat[5][5];
  private int price;
  private Random random = new Random();

  public Showtime() {
    // Initializing the each seat in seats array to avoid NullPointerException
    for (int i = 0; i < seats[0].length; i++) {
      for (int j = 0; j < seats[1].length; j++) {
        seats[i][j] = new Seat();
      }
    }
  }

  public Seat getSeat(int i, int j) {
    return seats[i][j];
  }

  /*
  * Setting all the seats to available in EndTheDay operations
  * */
  public void resetSeats() {
    for (Seat[] seats1 : seats) {
      for (Seat seat : seats1) {
        seat.setStatus('O');
      }
    }
  }

  /*
   * Resetting the prices in EndTheDay operations
   * */
  public void resetPrice() {
    price = 0;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    if (price > 0) {
      this.price = price;
    }
  }

  public void fillSeat(String seatPlace, char personType) {
    char seatChar = seatPlace.charAt(0);

    if (isSeatCharValid(seatChar)) {
      // Ready to use in seats array
      int seatNumber1 = convertCharToInt(seatChar);
      int seatNumber2 = Integer.valueOf(seatPlace.substring(1)) - 1;

      if (isSeatNumberValid(seatNumber1) && isSeatNumberValid(seatNumber2)) {
        // If the selected seat is empty, set 'X' or 'S' the status of the selected seat
        // But if it is not empty, gives a random seat which is empty
        if (seats[seatNumber1][seatNumber2].getStatus() == 'O') {
          if (personType == 'S') {
            seats[seatNumber1][seatNumber2].setStatus('S');
          } else {
            seats[seatNumber1][seatNumber2].setStatus('X');
          }
        } else {
          boolean flag = false;

          do {
            int ind1 = random.nextInt(5);
            int ind2 = random.nextInt(5);

            if (seats[ind1][ind2].getStatus() == 'O') {
              if (personType == 'S') {
                seats[ind1][ind2].setStatus('S');
              } else {
                seats[ind1][ind2].setStatus('X');
              }
              flag = true;
            }
          } while (!flag);
        }
      }
    }
  }

  /*
  * Deletes the seat which came from parameter
  * Also returns the status of the seat which is deleted
  * */
  public char emptySeat(String seatPlace) {
    char tempStatus = 'X';
    char seatChar = charToUpper(seatPlace.charAt(0));

    if (isSeatCharValid(seatChar)) {
      // Ready to use in seats array
      int seatNumber1 = convertCharToInt(seatChar);
      int seatNumber2 = Integer.valueOf(seatPlace.substring(1)) - 1;

      if (isSeatNumberValid(seatNumber1) && isSeatNumberValid(seatNumber2)) {
        if (seats[seatNumber1][seatNumber2].getStatus() != 'O') {
          tempStatus = seats[seatNumber1][seatNumber2].getStatus();
          seats[seatNumber1][seatNumber2].setStatus('O');
        }
      }
    }

    return tempStatus;
  }

  /*
  * Returning if the seat letter is valid or not
  * */
  private boolean isSeatCharValid(char seatChar) {
    return (seatChar == 'A' || seatChar == 'B' || seatChar == 'C' || seatChar == 'D' || seatChar == 'E');
  }

  /*
  * Returning if the seat number is valid or not
  * */
  private boolean isSeatNumberValid(int seatNumber) {
    return (seatNumber >= 0 && seatNumber < 5);
  }

  /*
  * Converts a char to uppercase
  * */
  private char charToUpper(char seatChar) {
    String seatStr = String.valueOf(seatChar).toUpperCase();
    seatChar = seatStr.charAt(0);
    return seatChar;
  }

  /*
  * Converts a char to number to use in array
  * 'A' -> 0
  * 'B' -> 1 ...
  * */
  private int convertCharToInt(char seatChar) {
    seatChar = charToUpper(seatChar);
    int seatAscii = (int) seatChar;
    return seatAscii - 65;
  }

}