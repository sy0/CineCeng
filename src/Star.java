public class Star {

  private String name;
  private String birthDay;
  private String birthPlace;
  private int starID;

  public Star(String name, String birthDay, String birthPlace) {
    this.name = name;
    this.birthDay = birthDay;
    this.birthPlace = birthPlace;
  }

  public String getName() {
    return name;
  }

  public String getBirthDay() {
    return birthDay;
  }

  public String getBirthPlace() {
    return birthPlace;
  }

  public int getStarID() {
    return starID;
  }

  public void setStarID(int starID) {
    this.starID = starID;
  }

}
