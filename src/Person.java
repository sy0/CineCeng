public class Person {

  private boolean isStudent;

  public Person(boolean isStudent) {
    this.isStudent = isStudent;
  }

  public boolean getIsStudent() {
    return isStudent;
  }

  public void setIsStudent(boolean isStudent) {
    this.isStudent = isStudent;
  }

}