public class Director {

  private String name;
  private String birthDay;
  private String birthPlace;
  private int directorID;

  public Director(String name, String birthDay, String birthPlace) {
    this.name = name;
    this.birthDay = birthDay;
    this.birthPlace = birthPlace;
  }

  public String getName() {
    return name;
  }

  public String getBirthDay() {
    return birthDay;
  }

  public String getBirthPlace() {
    return birthPlace;
  }

  public int getDirectorID() {
    return directorID;
  }

  public void setDirectorID(int directorID) {
    this.directorID = directorID;
  }

}