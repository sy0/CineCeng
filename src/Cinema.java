/*
* Enigma Console & IO File Reading
* */
import enigma.console.Console;
import enigma.console.TextWindow;
import enigma.core.Enigma;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Cinema {

  /*
  * Initializing the arrays of the cinema object by the given parameters.
  * */
  public Cinema(int lengthOfMovies, int lengthOfStars, int lengthOfDirectors, int lengthOfSaloons) {
    movies = new Movie[lengthOfMovies];
    stars = new Star[lengthOfStars];
    directors = new Director[lengthOfDirectors];
    saloons = new Saloon[lengthOfSaloons];
    // Initializing the saloons to avoid NullPointerException
    for (int i = 0; i < saloons.length; i++) {
      saloons[i] = new Saloon();
    }
    totalRevenue = 0.0;
  }

  // Enigma Console
  private Console console = Enigma.getConsole("CineCeng", 120, 40, 12, 1);
  private TextWindow consoleText = console.getTextWindow();

  // Saloons & Movies & Directors & Stars
  private Saloon[] saloons;
  private Movie[] movies;
  private Director[] directors;
  private Star[] stars;
  /*
  * To use in giving the IDs of the objects and indexes in array.
  * */
  private int indexMovies = 1;
  private int indexDirectors = 1;
  private int indexStars = 1;
  private double totalRevenue;

  /*
  * Starting the loop.
  * */
  public void start() {
    while (true) {
      read();
    }
  }

  /*
  * Reads the given input by user.
  * If it equals to "load", calls readFile method to parsing text file.
  * If not, parses the command.
  * */
  private void read() {
    String firstCommand = console.readLine();

    if (firstCommand.substring(0, 4).equals("load")) {
      readFile(firstCommand);
    } else if (firstCommand.equalsIgnoreCase("exit")) {
      System.exit(0);
    } else {
      parseCommand(firstCommand);
    }
  }

  /*
  * Reads the file and calls parseCommand method by giving lines one by one.
  * */
  private void readFile(String firstCommand) {
    String filePath = firstCommand.substring(5);

    // Reading input file
    BufferedReader bufferedReader = null;
    try {
      File file = new File(filePath);
      FileReader fileReader = new FileReader(file);
      bufferedReader = new BufferedReader(fileReader);
      String line;

      // Handling commands
      bufferedReader = new BufferedReader(fileReader);
      while ((line = bufferedReader.readLine()) != null) {
        parseCommand(line);
      }

    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        if (bufferedReader != null) {
          bufferedReader.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  /*
  * Parses the line if it equals any command in the cinema.
  * Calls the necessary method by command.
  * */
  private void parseCommand(String line) {
    String[] splittedLine = line.split(";");
    String command = splittedLine[0];

    if (command.equals("addStar")) {
      int lengthOfCommand = splittedLine.length;
      if (lengthOfCommand == 4) {
        String newName = splittedLine[1];
        String newBirthday = splittedLine[2];
        String newBirthPlace = splittedLine[3];
        Star newCelebrity = new Star(newName, newBirthday, newBirthPlace);
        try {
          addStar(newCelebrity);
        } catch (ArrayIndexOutOfBoundsException e) {
          e.printStackTrace();
        }
      }
    } else if (command.equals("addDirector")) {
      int lengthOfCommand = splittedLine.length;
      if (lengthOfCommand == 4) {
        String newName = splittedLine[1];
        String newBirthday = splittedLine[2];
        String newBirthPlace = splittedLine[3];
        Director newCelebrity = new Director(newName, newBirthday, newBirthPlace);
        try {
          addDirector(newCelebrity);
        } catch (ArrayIndexOutOfBoundsException e) {
          e.printStackTrace();
        }
      }
    } else if (command.equals("addMovie")) {
      int lengthOfCommand = splittedLine.length;
      /*
      * Controls the length to check if it is valid or not
      * Minimum number of stars: 2
      * Maximum number of stars: 10
      * */
      if (lengthOfCommand >= 7 && lengthOfCommand <= 14) {
        String newTitle = splittedLine[1];
        String newGenre = splittedLine[2];
        int newDuration = Integer.parseInt(splittedLine[3]);
        int newDirectorID = Integer.parseInt(splittedLine[4]);
        Director newDirector = directors[newDirectorID - 1];
        /*
        * Creates a newStars array to send with method as a parameter
        * Adds the all stars in the array by for loop
        * */
        Star[] newStars = new Star[lengthOfCommand - 5];
        for (int i = 5; i < lengthOfCommand; i++) {
          int newStarID = Integer.parseInt(splittedLine[i]);
          newStars[i - 5] = stars[newStarID];
        }
        Movie newMovie = new Movie(newTitle, newGenre, newDuration, newDirector, newStars);
        try {
          addMovie(newMovie);
        } catch (ArrayIndexOutOfBoundsException e) {
          e.printStackTrace();
        }
      }
    } else if (command.equals("updateSaloon")) {
      String saloonID = splittedLine[1];
      int movieID = Integer.parseInt(splittedLine[2]);
      updateSaloon(saloonID, movieID);
    } else if (command.equals("updateShowtime")) {
      String saloonID = splittedLine[1];
      String showtime = splittedLine[2];
      int price = Integer.parseInt(splittedLine[3]);
      updateShowtime(saloonID, showtime, price);
    } else if (command.equals("sellTicket")) {
      String saloonID = splittedLine[1];
      String showtime = splittedLine[2];
      String seat = splittedLine[3];
      String personType = splittedLine[4];
      Person person;

      if (personType.equals("student")) {
        person = new Person(true);
      } else {
        person = new Person(false);
      }

      sellTicket(saloonID, showtime, seat, person);
    } else if (command.equals("cancelTicket")) {
      String saloonID = splittedLine[1];
      String showtime = splittedLine[2];
      String seat = splittedLine[3];
      cancelTicket(saloonID, showtime, seat);
    } else if (command.equals("displayActors")) {
      displayActors();
    } else if (command.equals("displayDirectors")) {
      displayDirectors();
    } else if (command.equals("displayMovies")) {
      displayMovies();
    } else if (command.equals("displaySeats")) {
      displaySeats(splittedLine[1], splittedLine[2]);
    } else if (command.equals("endTheDay")) {
      endTheDay();
    }
  }

  /*
  * Adds a star to stars array.
  * Increases the indexStars.
  * */
  private void addStar(Star newStar) {
    newStar.setStarID(indexStars);
    stars[indexStars - 1] = newStar;
    indexStars++;
  }

  /*
   * Adds a director to directors array.
   * Increases the indexDirectors.
   * */
  private void addDirector(Director newDirector) {
    newDirector.setDirectorID(indexDirectors);
    directors[indexDirectors - 1] = newDirector;
    indexDirectors++;
  }

  /*
   * Adds a movie to movies array.
   * Increases the indexMovies.
   * */
  private void addMovie(Movie newMovie) {
    newMovie.setMovieID(indexMovies);
    movies[indexMovies - 1] = newMovie;
    indexMovies++;
  }

  /*
  * Determines which movie will be shown in the saloon.
  * */
  private void updateSaloon(String saloonID, int movieID) {
    int ID = Integer.valueOf(saloonID.substring(1)) - 1;
    saloons[ID].setMovie(movies[movieID]);
  }

  /*
   * Determines the price of the movie which will be shown in the saloon.
   * */
  private void updateShowtime(String saloonID, String showtime, int price) {
    int ID = Integer.valueOf(saloonID.substring(1)) - 1;
    saloons[ID].getShowtime(showtime).setPrice(price);
  }

  /*
  * Sells the ticket which is selected by user.
  * Increases the totalRevenue and revenue of the movie of the selected saloon by the price of the showtime.
  * If the person is student price will be dropped by half.
  * */
  private void sellTicket(String saloonID, String showtime, String seat, Person person) {
    char personType = (person.getIsStudent()) ? 'S' : 'X';
    int ID = Integer.valueOf(saloonID.substring(1)) - 1;
    saloons[ID].getShowtime(showtime).fillSeat(seat, personType);
    int price = saloons[ID].getShowtime(showtime).getPrice();
    if (personType == 'S') {
      totalRevenue += (double) price / 2;
      saloons[ID].getMovie().increaseRevenue((double) price / 2);
    } else {
      totalRevenue += (double) price;
      saloons[ID].getMovie().increaseRevenue((double) price);
    }
  }

  /*
   * Cancels the ticket which is selected by user.
   * Decreases the totalRevenue and revenue of the movie of the selected saloon by the price of the showtime.
   * */
  private void cancelTicket(String saloonID, String showtime, String seat) {
    int ID = Integer.valueOf(saloonID.substring(1)) - 1;
    int price = saloons[ID].getShowtime(showtime).getPrice();
    char statusOfDeletedSeat = saloons[ID].getShowtime(showtime).emptySeat(seat);
    if (statusOfDeletedSeat == 'S') {
      totalRevenue -= (double) price / 2;
      saloons[ID].getMovie().decreaseRevenue((double) price / 2);
    } else {
      totalRevenue -= (double) price;
      saloons[ID].getMovie().decreaseRevenue((double) price);
    }
  }

  /*
  * Displays all actors.
  * */
  private void displayActors() {
    consoleText.output("\n\nSTARS\n");
    for (int i = 0; i < stars.length; i++) {
      if (stars[i] != null) {
        consoleText.output(stars[i].getStarID() + ". " + stars[i].getName() + " - " + stars[i].getBirthDay() + " - " + stars[i].getBirthPlace() + "\n");
      }
    }
  }

  /*
  * Displays all directors.
  * */
  private void displayDirectors() {
    consoleText.output("\n\nDIRECTORS\n");
    for (int i = 0; i < directors.length; i++) {
      if (directors[i] != null) {
        consoleText.output(directors[i].getDirectorID() + ". " + directors[i].getName() + " - " + directors[i].getBirthDay() + " - " + directors[i].getBirthPlace() + "\n");
      }
    }
  }

  /*
  * Dispalys all movies.
  * */
  private void displayMovies() {
    consoleText.output("\n\nMOVIES\n");
    for (int i = 0; i < indexMovies - 1; i++) {
      consoleText.output(movies[i].getMovieID() + ". " + movies[i].getTitle() + " - " + movies[i].getGenre() + " - Duration: " + movies[i].getDuration() + "m - Director: " + movies[i].getDirector().getName() + "\n");
      consoleText.output("\tStars: ");
      for (int k = 0; k < movies[i].getStars().length; k++) {
        consoleText.output(stars[k].getName() + " | ");
      }
      consoleText.output("\n");
    }
  }

  /*
  * Display the seat which is selected by user.
  * */
  private void displaySeats(String saloonID, String showtime) {
    int ID = Integer.valueOf(saloonID.substring(1)) - 1;
    consoleText.output("\n");
    consoleText.output((ID + 1) + ". Saloon | Showtime: " + showtime.substring(0, 1).toUpperCase() + showtime.substring(1) + "\n");

    for (int i = 0; i < 5; i++) {
      for (int j = 0; j < 5; j++) {
        char seat = saloons[ID].getShowtime(showtime).getSeat(i, j).getStatus();
        consoleText.output((seat == 'S') ? 'X' : seat);
      }
      consoleText.output("\n");
    }
  }

  /*
  * Displays all the statistics of the day when endTheDay command is entered.
  * Also calls the resetTheDay method to be ready for next day.
  * */
  private void endTheDay() {
    // Display of statistics of the day
    consoleText.output("\nThe most-watched movie: " + mostWatchedMovie() + "\n");
    consoleText.output("Total revenue of CineCeng: " + totalRevenue + "\n");
    consoleText.output("The most-filled showtime: " + mostFilledShowtime() + "\n");
    consoleText.output("The highest-income movie: " + getHighestIncomeMovie() + "\n");
    displayAllSeats();

    resetTheDay();
  }

  /*
  * Display all seats in the end of the day.
  * */
  private void displayAllSeats() {
    consoleText.output("\n\n\t\t\ts1\t\ts2\t\ts3");
    consoleText.output("\n\nMorning \t");

    for (int i = 0; i < 5; i++) {
      for (int k = 0; k < 3; k++) {
        for (int j = 0; j < 5; j++) {
          char seat = saloons[k].getShowtime("morning").getSeat(i, j).getStatus();
          consoleText.output((seat == 'S') ? 'X' : seat);
        }
        consoleText.output("\t");
      }
      consoleText.output("\n\t\t\t");
    }

    consoleText.output("\n\nNoon    \t");

    for (int i = 0; i < 5; i++) {
      for (int k = 0; k < 3; k++) {
        for (int j = 0; j < 5; j++) {
          char seat = saloons[k].getShowtime("noon").getSeat(i, j).getStatus();
          consoleText.output((seat == 'S') ? 'X' : seat);
        }
        consoleText.output("\t");
      }
      consoleText.output("\n\t\t\t");
    }

    consoleText.output("\n\nEvening \t");

    for (int i = 0; i < 5; i++) {
      for (int k = 0; k < 3; k++) {
        for (int j = 0; j < 5; j++) {
          char seat = saloons[k].getShowtime("evening").getSeat(i, j).getStatus();
          consoleText.output((seat == 'S') ? 'X' : seat);
        }
        consoleText.output("\t");
      }
      consoleText.output("\n\t\t\t");
    }

    consoleText.output("\n\n");
  }

  /*
  * Finds the most watched movie in all showtimes in a day.
  * Returns the name of the movie
  * */
  private String mostWatchedMovie() {
    String mostWatchedMovie = null;
    int mostWatchedCount = 0;
    Movie[] watchedMovies = new Movie[3];

    for (int i = 0; i < saloons.length; i++) {
      Movie movie = saloons[i].getMovie();
      for (int j = 0; j < watchedMovies.length; j++) {
        if (watchedMovies[j] == movie) {
          break;
        }
        watchedMovies[i] = movie;
      }
    }

    int[] watchedCountSaloon = new int[saloons.length];

    for (int i = 0; i < saloons.length; i++) {
      Showtime morning = saloons[i].getShowtime("morning");
      Showtime noon = saloons[i].getShowtime("noon");
      Showtime evening = saloons[i].getShowtime("evening");

      for (int j = 0; j < 5; j++) {
        for (int k = 0; k < 5; k++) {
          Seat seat = morning.getSeat(j, k);
          if (seat.getStatus() == 'X' || seat.getStatus() == 'S') {
            watchedCountSaloon[i]++;
          }
        }
      }
      for (int j = 0; j < 5; j++) {
        for (int k = 0; k < 5; k++) {
          Seat seat = noon.getSeat(j, k);
          if (seat.getStatus() == 'X' || seat.getStatus() == 'S') {
            watchedCountSaloon[i]++;
          }
        }
      }
      for (int j = 0; j < 5; j++) {
        for (int k = 0; k < 5; k++) {
          Seat seat = evening.getSeat(j, k);
          if (seat.getStatus() == 'X' || seat.getStatus() == 'S') {
            watchedCountSaloon[i]++;
          }
        }
      }
      if (watchedCountSaloon[i] > mostWatchedCount) {
        mostWatchedCount = watchedCountSaloon[i];
        mostWatchedMovie = saloons[i].getMovie().getTitle();
      }
    }

    return mostWatchedMovie;
  }

  /*
  * Finds the most filled showtime in a day.
  * Returns saloon and showtime.
  * */
  private String mostFilledShowtime() {
    String mostFilledShowtime;
    String showtime;
    int maxSeatCounter = 0;
    int seatCounter = 0;
    int maxSaloonId = 0;
    int maxShowtimeId = 0;

    for (int i = 0; i < saloons.length; i++) {
      Showtime[] showtimes = new Showtime[3];
      showtimes[0] = saloons[i].getShowtime("morning");
      showtimes[1] = saloons[i].getShowtime("noon");
      showtimes[2] = saloons[i].getShowtime("evening");
      for (int j = 0; j < showtimes.length; j++) {
        for (int k = 0; k < 5; k++) {
          for (int l = 0; l < 5; l++) {
            char seat = showtimes[j].getSeat(k, l).getStatus();
            if (seat != 'O') {
              seatCounter++;
            }
          }
        }
        if (seatCounter > maxSeatCounter) {
          maxSeatCounter = seatCounter;
          maxSaloonId = i + 1;
          maxShowtimeId = j;
        }
        seatCounter = 0;
      }
    }

    if (maxShowtimeId == 0) {
      showtime = "morning";
    } else if (maxShowtimeId == 1) {
      showtime = "noon";
    } else if (maxShowtimeId == 2) {
      showtime = "evening";
    } else {
      showtime = " ";
    }

    mostFilledShowtime = "s" + maxSaloonId + "-" + showtime;
    return mostFilledShowtime;
  }

  /*
  * Finds the highest-income movie in all showtimes in a day.
  * Returns the name of the movie and revenue of that showtime.
  * */
  private String getHighestIncomeMovie() {
    String highestIncomeMovie = null;
    double maxIncome = 0;
    for (Movie movie : movies) {
      if (movie != null && movie.getRevenue() > maxIncome) {
        maxIncome = movie.getRevenue();
        highestIncomeMovie = "\"" + movie.getTitle() + "\" - " + maxIncome;
      }
    }

    return highestIncomeMovie;
  }

  /*
  * Resets the all necessary things which will be used in next day.
  * */
  private void resetTheDay() {
    totalRevenue = 0;

    for (Saloon saloon : saloons) {
      saloon.resetMovie();
      saloon.getShowtime("morning").resetPrice();
      saloon.getShowtime("noon").resetPrice();
      saloon.getShowtime("evening").resetPrice();
      saloon.getShowtime("morning").resetSeats();
      saloon.getShowtime("noon").resetSeats();
      saloon.getShowtime("evening").resetSeats();
    }

    for (Movie movie : movies) {
      if (movie != null) {
        movie.resetRevenue();
      }
    }
  }

}